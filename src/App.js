import React, { Component } from 'react';
import logo from './logo.svg';

import Layout from './layout';
import Page from './layout/components/Page';


import './App.css';

class App extends Component {
  render() {
    return (
      <Layout>
        {Page(this.props.children)}
      </Layout>
    );
  }
}

export default App;
