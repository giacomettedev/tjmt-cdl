import HttpService from '../services/http.service';
import faker from 'faker';


function criarItems(n) {
    var items = [];

    for (var i = 0; i < n; i++) {
        items.push({
            nome: faker.name.findName(),
            data: faker.date.past(20, new Date(2000, 10, 1))
        }); 
    }


    return items;
}

export default class ProcessoApi {

    constructor() {
        this.http = new HttpService();
    }

    getProcesso(numeroUnico) {

        return this.http.get(`/processo?numeroUnico=${numeroUnico}`);

    }

}