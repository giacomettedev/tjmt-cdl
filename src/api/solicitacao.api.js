import HttpService from '../services/http.service';
import faker from 'faker';


function criarItems(n) {
    var items = [];

    for (var i = 0; i < n; i++) {
        items.push({
            nome: faker.name.findName(),
            data: faker.date.past(20, new Date(2000, 10, 1))
        }); 
    }


    return items;
}

export default class SolicitacaoApi {

    constructor() {
        this.http = new HttpService();
    }

    getSolicitacoes() {


        return new Promise(function (resolve, reject) {

            setTimeout(() => resolve([
                {
                    title: "Pendentes",
                    icon: "clock-alert",
                    items: criarItems(faker.random.number(10))
                },
                {
                    title: "Recentemente Concluidos",
                    icon: "checkbox-marked-circle-outline",
                    items: criarItems(faker.random.number(10))
                },
            ]), Math.round(Math.random() * 1000) + 1200);

        });

    }

}