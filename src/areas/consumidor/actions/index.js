import ConsumidorApi from 'api/consumidor.api';

const api = new ConsumidorApi();

export function requestConsumidores(search){
    return {
        type: "FETCH_CONSUMIDORES_REQUEST",
        search,
        consumidores: []
    };
}

export function receiveConsumidores(error, consumidores){
    var selecionado = null;
    if(consumidores && consumidores.length == 1){
        selecionado = consumidores[0].id;
    }

    return {
        type: "FETCH_CONSUMIDORES_" + (error ? "FAILURE" : "SUCCESS"),
        error,
        consumidores,
        selecionado
    };
}


export function selecionarConsumidor(id){
    return {
        type: "CONSUMIDOR_SELECIONADO",
        selecionado: id
    };
}

export function fetchConsumidores(search){

    return function(dispatch){

        dispatch(requestConsumidores(search));

        api.getConsumidores(search)
            .then( consumidores => dispatch(receiveConsumidores(null, consumidores)) )
            .catch( err => dispatch(receiveConsumidores(err, null)));

    };

}