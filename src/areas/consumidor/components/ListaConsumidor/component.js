import React from 'react';
import ConsumidorApi from 'api/consumidor.api';


import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider'; 

import { Container, Row, Col } from 'reactstrap';
import {withRouter} from 'react-router';

import RaisedButton from 'material-ui/RaisedButton';

 
class ListaConsumidores extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            mostrarTodos: false
        };
    }

    itemStyle(i){
        return {
            innerDivStyle: {
                padding: "8px 16px",                
                fontWeight: i == null ? 300 : 500
            },
            className: i % 2 ? "alternate" : ""
        };
    }

    handleSelecionarCinsumidor(id){
         this.setState({ mostrarTodos: false }) 
         this.props.selecionarConsumidor(id);
    }

    get items(){
        debugger;
        let {consumidores, selecionado } = this.props;
        let quantidade = consumidores.length;
        let quantidadeOculta = quantidade - 2;
        let mostrarVerTodos = false;

        if ( (selecionado && !this.state.mostrarTodos) && quantidade > 3) {
            consumidores = consumidores.slice(0, 2);
            mostrarVerTodos = true;
        }

        return consumidores.map((item, i) => 
            <ListItem 
                onClick={() => this.handleSelecionarCinsumidor(item.id) } 
                {...this.itemStyle(i)} 
                key={item.id}>
                <i className="mdi mdi-account" /> {item.nome}
            </ListItem> 
                
            ).concat(this.verTodos(mostrarVerTodos, quantidadeOculta))
            
    }

    verTodos(mostrar, quantidade){
        if(!mostrar) return null;

        return (
            <ListItem
                onClick={() => this.setState({ mostrarTodos: true }) }
                {...this.itemStyle(null)}
                key={-1} >
                    <i className="mdi mdi-chevron-down" /> Ver os outros {quantidade} encontrados                
            </ListItem>
        );
    }

    render() {

        let {consumidores, selecionado} = this.props;

        if (!consumidores || consumidores.length <= 1) return null;
 
        let quantidade = consumidores.length;

        return (

            <Row>
                <Col lg="4" md="6" sm="12" >          
                    <List >
                        <Subheader  >Foram encontrados {quantidade} consumidores.</Subheader>
                        {this.items}                     
                    </List>
                </Col>
            </Row>

        );

    }

}

export default withRouter(ListaConsumidores);