import {fetchConsumidores, selecionarConsumidor} from '../../actions';
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({
    consumidores: state.consumidor.consumidores || [],
    selecionado: state.consumidor.selecionado 
});

const mapDispatchToProps = (dispatch) => ({
    selecionarConsumidor(id){
        dispatch(selecionarConsumidor(id))
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);