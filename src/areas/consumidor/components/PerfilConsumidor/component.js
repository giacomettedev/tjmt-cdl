import React from 'react';
import moment from 'moment';

import style from './style.usable.css';

import ConsumidorApi from 'api/consumidor.api';

import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import Subheader from 'material-ui/Subheader';


import { Container, Row, Col } from 'reactstrap';

import InfoTable from './info-table';


class PerfilConsumidor extends React.Component {

    get containerStyle() {

        return {
            padding: 18
        };

    }

    componentWillMount() {
       style.use();
    }

    componentWillUnmount() {
        style.unuse();
    }


    render() {

        const {consumidor} = this.props;

        if (!consumidor) return null;

        return (

            <Row className="profile">
                <Col  md="8" sm="12" >

                    <div style={this.containerStyle} >
                        <h1>Perfil do Consumidor</h1>
                        <h1 className="person-name">
                            <i className="mdi mdi-account" />
                            {this.props.consumidor.nome}
                        </h1>
                        <InfoTable consumidor={this.props.consumidor} />

                        <RaisedButton                             
                            primary
                            style={{ margin: "16px 0" }}                             
                            label={[<i className="mdi mdi-tooltip-outline-plus" />, " Realizar Solicitação"]}  />


                    </div>
                    <Divider />

                </Col>
            </Row>

        );

    }

}

export default PerfilConsumidor;