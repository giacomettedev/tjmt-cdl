import {fetchConsumidores, selecionarConsumidor} from '../../actions';
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({    
    consumidor: (state.consumidor.consumidores || []).filter( x => x.id == state.consumidor.selecionado)[0] 
});

const mapDispatchToProps = (dispatch) => ({
    selecionarConsumidor(id){
        dispatch(selecionarConsumidor(id))
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);