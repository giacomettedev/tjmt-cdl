import React from 'react';

import {withRouter} from 'react-router';


class DetailPage extends React.Component {

    navToBack(){
        this.props.router.push('/');
    }

    render(){

        return <h1 onClick={() => this.navToBack()}> Detail Page </h1>;

    }

}

export default withRouter(DetailPage);