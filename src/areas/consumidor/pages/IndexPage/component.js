import React from 'react'; 

import {withRouter} from 'react-router';

import {Input} from 'reactstrap';
import RaisedButton from 'material-ui/RaisedButton';

import ListaConsumidor from '../../components/ListaConsumidor';
import PerfilConsumidor from '../../components/PerfilConsumidor';
import withCardPage from 'layout/components/CardPage';


class IndexPage extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            search: ""
        }
        
    }
 

    link(prop){
        return {
            value: this.state[prop],
            onChange: (e) => this.setState({ [prop]: e.target.value })
        }
    }

    render(){
 

        return (
            <div>
                <div style={{ margin: "10px" }}>

                    <Input 
                        {...this.link("search")} 
                        disabled={this.props.isLoading} 
                        type="text" 
                        placeholder="Pesquisa de CPF" 
                        style={{ width: "auto", display: "inline-block", margin: "4px" }} />

                    <RaisedButton 
                        disabled={this.props.isLoading} 
                        label={this.props.isLoading ? "Carregando..." : "Pesquisar"} 
                        primary onClick={() => this.props.requestConsumidores(this.state.search)} /> 

                </div>
                
                <PerfilConsumidor />
                <ListaConsumidor  />
            </div>
        );

    }

}

export default withCardPage(withRouter(IndexPage));