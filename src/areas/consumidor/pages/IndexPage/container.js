import {fetchConsumidores} from '../../actions';
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({
     isLoading: state.consumidor.fetching
});

const mapDispatchToProps = (dispatch) => ({
    requestConsumidores(s){
        dispatch(fetchConsumidores(s))
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);