export function consumidor(state = {}, {type, error, consumidores, selecionado}){

    switch(type){

        case "FETCH_CONSUMIDORES_REQUEST":
            return { ...state, fetching: true, selecionado: null, consumidores };
        case "FETCH_CONSUMIDORES_SUCCESS":
            return { ...state, fetching: false, error: null, consumidores, selecionado };        
        case "FETCH_CONSUMIDORES_FAILURE":
            return { ...state, fetching: false, error, consumidores: [], selecionado: null };
        case "CONSUMIDOR_SELECIONADO":
            return { ...state, selecionado };

    }

    return state;

}
