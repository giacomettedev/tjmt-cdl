import {setLoading} from 'layout/actions';
import ProcessoApi from 'api/processo.api';


const api = new ProcessoApi();

export function showDetalhesParte(detalhesParte) {
    return {
        type: "VISUALIZAR_SOLICITACAO_PROCESSO_PARTE_DETALHES",
        detalhesParte
    };
}

export function selecionarParte(parte, selecionarParte) {
    return {
        type: "SELECIONAR_SOLICITACAO_PROCESSO_PARTE",
        [parte.idPoloProcessual == 1 ? "autor" : "reu" ]: parte,
        selecionarParte        
    };
}

export function setMotivo(motivo){
    return {
        type: "SET_SOLICITACAO_MOTIVO",
        motivo
    };
}

export function setTipo(tipo){
    return {
        type: "SET_SOLICITACAO_TIPO",
        tipo
    };
}

export function requestProcesso(numeroUnico) {
    return {
        type: "FETCH_SOLICITACAO_PROCESSO_REQUEST",
        numeroUnico,
        partes: [],
        processo: null
    };
}

export function receiveProcesso(error, processo) {
    if (processo) {
        var partes = processo.partes;
    }

    if(!error && !processo){
        error = "Processo não encontrado.";
    }

    if (error && typeof error == "object") {
        error = error.message || "Ocorreu um erro";
    }

    return {
        type: "FETCH_SOLICITACAO_PROCESSO_" + (error ? "FAILURE" : "SUCCESS"),
        error,
        partes,
        processo
    };
}

export function clearProcesso() {
    return {
        type: "CLEAR_SOLICITACAO_PROCESSO"
    };
}

export function fetchProcesso(numeroUnico) {

    return function (dispatch) {


        if (!/[0-9]{1,}-[0-9]{1,2}\.(19[98][0-9]|20[01][0-9])\.[0-9]\.?[0-9]{2}\.[0-9]{4}/.test(numeroUnico)) {
            dispatch(receiveProcesso("É necessário informar um número único válido", null));
            return;
        }

        dispatch(setLoading(true))
        dispatch(requestProcesso(numeroUnico));


        api.getProcesso(numeroUnico)
            .then(data => {
                dispatch(setLoading(false))                
                dispatch(receiveProcesso(null, data))
            })
            .catch(err => {
                dispatch(setLoading(false))
                dispatch(receiveProcesso(err, null))
            });

    };

}