import React from 'react';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Chip from 'material-ui/Chip';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import {Col, Row, Input, Label} from 'reactstrap';
import ColCard from 'layout/components/ColCard';
import linkable from 'shared/components/linkable';
import LoadingDialog from 'shared/components/LoadingDialog';
import ChipsPartes from '../ChipsPartes';

export default class FormularioSolicitado extends React.Component {

    constructor(props) {
        super(props);
        this.link = linkable.bind(this);

        this.state = {

        };
    }


    renderTipo() {

        return (
            <Row>
                <Col md="8" lg="6" style={{ marginTop: "16px" }} >

                    <Label className="text-info">Sua solicitação sera de invclusão ou remoção de Negativação?</Label><br/>
                    <SelectField {...this.link("tipoSolicitacao") } hintText="Escolha o tipo da Solicitação" style={{ marginTop: "-8px" }} >
                        <MenuItem value="1" primaryText="Inclusão de Negativação" />
                        <MenuItem value="2" primaryText="Remoção de Negativação" />
                    </SelectField>
                </Col>
            </Row>
        );

    }

    renderMotivo() {
        const {motivo, setMotivo} = this.props;

        return (
            <Row>
                <Col md="8" lg="6" style={{ marginTop: "4px" }} >
                    <Label  className="text-info" for="motivo">Qual o motivo da solicitação?</Label>
                    <Input value={motivo} onChange={e => setMotivo(e.target.value)} id="motivo" placeholder="Motivo" type="textarea" />
                </Col>
            </Row>
        );
    }

    render() {


        var {processo, reusSelecionados, autoresSelecionados, partesInvalidas} = this.props;


        if (!processo) return null;

        var hasInvalidos = !!partesInvalidas.length;

        return (
            <div>
                <LoadingDialog />
                <ColCard>
                    <h1>Dados da Solicitação</h1>

                    <ChipsPartes partes={reusSelecionados} tipoTexto="Réu" />
                    <ChipsPartes partes={autoresSelecionados} tipoTexto="Autora" />

                    {this.renderTipo()}
                    {this.renderMotivo()}

                    <Row style={{ textAlign: "right" }}>
                        <div style={{ textAlign: "left", marginTop: "16px" }}>
                            {hasInvalidos ?
                                <p className="text-danger">
                                    <i className="mdi mdi-alert" style={{ margin: "0 4px" }} />
                                    As partes destacadas em vermelho estão com dados incompletos, é necessário completar esses dados no <strong>sistema de origem do processo</strong>.

                                </p> : null }
                        </div>
                        <RaisedButton disabled={hasInvalidos} primary label="Realizar Solicitação" />
                    </Row>
                </ColCard>
            </div>
        );

    }

}