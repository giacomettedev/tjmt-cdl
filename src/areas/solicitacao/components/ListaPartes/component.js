import React from 'react';
import moment from 'moment';
import style from './style.usable.css';
import {withRouter} from 'react-router';

import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import Checkbox from 'material-ui/Checkbox';
import {Col} from 'reactstrap';
import ColCard from 'layout/components/ColCard';

import ColumnPartes from './column';

// var iconStyle = {  
//     width: 72,
//     height: 72,
//     padding: 8,  
//     top: -8
// };

class ListaPartes extends React.Component {

    componentWillMount() {
        style.use();
    }

    componentWillUnount() {
        style.unuse();
    }

    showDetailsIcon() {
        return (
            <IconButton   >
                <FontIcon className="mdi mdi-magnify" />
            </IconButton>
        );
    }

    render() {
debugger;
        const {autoras, reus, autoresSelecionados, reusSelecionados} = this.props;

        if(!autoras.length || !reus.length ) return null;

        var partes = [
            { partes: autoras, title: "Autora", tipo: 1, selecionados: autoresSelecionados },
            { partes: reus, title: "Ré" , tipo: 2, selecionados: reusSelecionados },
        ]

        return (
            <div className="lista-de-partes"> 
                {partes.map( grupo => <ColumnPartes 
                        selecionarParte={this.props.selecionarParte} 
                        showDetalhesParte={this.props.showDetalhesParte} {...grupo} />)}                
            </div>
        );
    }

}

export default withRouter(ListaPartes);