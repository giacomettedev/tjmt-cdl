import {fetchProcesso, clearProcesso, showDetalhesParte, selecionarParte} from '../../actions';
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => {

    var partes = state.solicitacao.partes || [];
    var autoras = partes.filter(x => x.idPoloProcessual == 1);
    var reus = partes.filter(x => x.idPoloProcessual == 2);

    return {
        autoras,
        reus,
        autoresSelecionados: state.solicitacao.autoresSelecionados || [],
        reusSelecionados: state.solicitacao.reusSelecionados || [],
    };
};

const mapDispatchToProps = (dispatch) => ({
    showDetalhesParte(numeroUnico) {
        dispatch(showDetalhesParte(numeroUnico))
    },
    selecionarParte(parte, selecionar){
        dispatch(selecionarParte(parte, selecionar))
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);


