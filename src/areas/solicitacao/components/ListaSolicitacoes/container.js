import {fetchSolicitacoes} from '../../actions';
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({
    grupos: state.solicitacoes.grupos || [] 
});

const mapDispatchToProps = (dispatch) => ({
    
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);


