import React from 'react';
import moment from 'moment';
import {withRouter} from 'react-router';
import style from './style.usable.css';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import FontIcon from 'material-ui/FontIcon';

import faker from 'faker';

import {Card, CardBlock, CardTitle, CardSubtitle} from 'reactstrap';

function criarItems(n) {
    var items = [];

    for (var i = 0; i < n; i++) {
        items.push({
            nome: faker.name.findName(),
            data: faker.date.past(20, new Date(2000, 10, 1))
        });
    }


    return items;
}

class ListaSolicitacoes extends React.Component {

    componentWillMount() {
        style.use();
    }

    componentWillUnount() {
        style.unuse();
    }

    navTo(){
        this.props.router.push("solicitacoes/details");
    }

    render() {
        const {items, title} = this.props;


        if(!items || !items.length) {
            return <em style={{ padding: '12px', display: 'block'}}>Sem Items {title}</em>;    
        }


        return (
           <div>
            {items.map( ({nome, data}, i) => {
                    return <ListItem
                            onClick={() => this.navTo()}
                            primaryText={nome}
                            className={i % 2 ? "alternate" : ""}
                            secondaryText={moment(data).format("DD/MM/YYYY")}  
                            leftIcon={<FontIcon className="mdi mdi-file-document" />}  />

                })} 
            </div>
        );
    }

}

export default withRouter(ListaSolicitacoes);