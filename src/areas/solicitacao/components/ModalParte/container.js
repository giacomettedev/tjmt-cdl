import {showDetalhesParte} from '../../actions';
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({
    parteSelecionada: state.solicitacao.detalhesParte
});

const mapDispatchToProps = (dispatch) => ({
     
    close() {
        dispatch(showDetalhesParte())
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);


