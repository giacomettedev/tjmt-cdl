import React from 'react';



export default (props) => {

    const {title, value, isRequired} = props;

    const className = isRequired && !value ? "danger" : "";

    return (
        <tr className={className} >
            <th>{title}</th>
            <td className="info">{
                value || 
                
                (isRequired ? <span style={{ right: 8, position: "absolute" }} ><i className="mdi mdi-alert pull-right" /></span> : "--")
                
            }</td>
        </tr>
    );

};