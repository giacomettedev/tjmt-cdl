import React from 'react';
import moment from 'moment';

import style from './style.usable.css';
import InfoRow from './info-row';

class InforTable extends React.Component {

    componentWillMount() {
        style.use();
    }

    componentWillUnmount() {
        style.unuse();
    }

    get containerStyle() {

        return {
            padding: 18
        };

    }

    render() {

        const {parte} = this.props;

        if (!parte) return null;
        var parteNormalizada = {...parte };

        var sexo = (parte.sexo || "").toLowerCase();
        sexo = sexo == "m" ? "Masculino" : sexo == "f" ? "Feminino" : "";
        var telefone = parte.telefone;
        var dataNascimento = parte.dataNascimento ? moment(parte.dataNascimento).format("DD/MM/YYYY") : "";
        var cidade = parte.cidade ? parte.cidade + (parte.uf ? " - " + parte.uf : "") : "";
        var filiacao = !parte.nomeMae && !parte.nomePai ? "" : !parte.nomeMae || !parte.nomePai ? `${parte.nomePai}${parte.nomeMae}` : (<span>{parte.nomePai}<br/>{parte.nomeMae}</span>);

        return (
            <table className="profile">
                <InfoRow title="Nome" value={parte.nome} isRequired />
                <InfoRow title="CPF" value={parte.cpf} isRequired />
                <InfoRow title="RG" value={parte.rg} isRequired />
                <InfoRow title="Sexo" value={sexo} isRequired />
                <InfoRow title="Telefone" value={telefone} />
                <InfoRow title="Data de Nascimento" value={dataNascimento} isRequired />
                <InfoRow title="Filiação" value={filiacao} isRequired />
                <InfoRow title="Endereço" value={parte.endereco} isRequired /> 
                <InfoRow title="Bairro" value={parte.bairro}   />  
                <InfoRow title="Municipio" value={cidade}   />
            </table >

        );

    }

}

export default InforTable;


