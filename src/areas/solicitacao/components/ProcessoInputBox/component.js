import React from 'react'; 



import {Input} from 'reactstrap';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';


class InputBox extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            numeroUnico: ""
        };

    } 


    link(prop) {
        return {
            value: this.state[prop],
            onChange: (e) => this.setState({ [prop]: e.target.value })
        }
    }

    clearProcesso(){
        this.setState({ numeroUnico: '' });
        this.props.clearProcesso();
    }

    render() {

        var {numeroUnico} = this.state;
        const {isLoading,  loadProcesso, errorMessage} = this.props;
        const numeroUnicoProcesso= this.props.numeroUnico;

        numeroUnico = numeroUnicoProcesso || numeroUnico;

        return (
            <div>
                <div className="request-box">
                    <p className="text-info" >
                        Informe o número unico do processo no qual será realizada a solicitação.
                    </p>
                    <Input
                        {...this.link("numeroUnico") }
                        disabled={isLoading || numeroUnicoProcesso}
                        type="text"
                        placeholder="Numero Único"
                        className="processo-input" />

                    {  numeroUnicoProcesso ?
                            <RaisedButton
                                disabled={isLoading}
                                icon={<FontIcon className="mdi mdi-close" />}
                                label={"Limpar"}
                                onClick={() => this.clearProcesso() } /> 
                        : 
                            <RaisedButton
                                disabled={isLoading}
                                label={ "Pesquisar"}
                                primary onClick={() => loadProcesso(numeroUnico) } />
                    }

                    

                    <p className="text-danger">{errorMessage}</p>
                </div>
                
            </div>
        );

    }

}



export default InputBox;