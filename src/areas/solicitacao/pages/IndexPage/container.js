import {fetchSolicitacoes} from '../../actions'; 
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({ 
    
});

const mapDispatchToProps = (dispatch) => ({
    loadSolicitacoes(){
        dispatch(fetchSolicitacoes())
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);


