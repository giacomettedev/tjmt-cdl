import {fetchProcesso, clearProcesso} from '../../actions'; 
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({ 
    isLoading: state.solicitacao.fetching
});

const mapDispatchToProps = (dispatch) => ({
    loadProcesso(numeroUnico){
        dispatch(fetchProcesso(numeroUnico))
    },
    clearProcesso(){
        dispatch(clearProcesso())
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);


