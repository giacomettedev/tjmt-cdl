function verificarInvalida(parte) {


    return (
        !parte ||
        !parte.nome ||
        !parte.cpf ||
        !parte.rg ||
        !parte.sexo ||
        //      !parte.telefone ||
        !parte.dataNascimento ||
        !parte.nomeMae ||
        !parte.nomePai ||
        !parte.endereco ||
        //	    !parte.bairro ||
        !parte.cidade ||
        !parte.uf
    );

}

export function solicitacao(state = { reusSelecionados: [], autoresSelecionados: [] },
    {type, error, processo, partes, detalhesParte, autor, reu, selecionarParte, motivo, tipo}) {

    switch (type) {

        case "SET_SOLICITACAO_MOTIVO":
            return {...state, motivo };
        case "SET_SOLICITACAO_TIPO":
            return {...state, tipo };

        case "FETCH_SOLICITACAO_PROCESSO_REQUEST":
            return { 
                ...state,
                fetching: true,
                processo,
                partes,
                error: null,
                partesInvalidas: [],
                autoresSelecionados: [],
                reusSelecionados: [],
                tipo: 0,
                motivo: ""
            };
        case "FETCH_SOLICITACAO_PROCESSO_SUCCESS":
            partes && partes.forEach(x => x.invalida = verificarInvalida(x));
            return { ...state, fetching: false, error: null, processo, partes };
        case "FETCH_SOLICITACAO_PROCESSO_FAILURE":
            return { ...state, fetching: false, error, processo: null, partes: [] };
        case "CLEAR_SOLICITACAO_PROCESSO":
            return {...state, fetching: false, error: null, processo: null, partes: [] };
        case "VISUALIZAR_SOLICITACAO_PROCESSO_PARTE_DETALHES":
            return {...state, detalhesParte };
        case "SELECIONAR_SOLICITACAO_PROCESSO_PARTE":
            var state = {...state };

            if (selecionarParte) {
                if (autor) state.autoresSelecionados = (state.autoresSelecionados || []).concat(autor);
                if (reu) state.reusSelecionados = (state.reusSelecionados || []).concat(reu);
            } else {
                if (autor) state.autoresSelecionados = (state.autoresSelecionados || []).filter(x => x.id != autor.id);
                if (reu) state.reusSelecionados = (state.reusSelecionados || []).filter(x => x.id != reu.id);
            }
            state.partesInvalidas = state.autoresSelecionados.concat(state.reusSelecionados).filter(verificarInvalida);
            return state;
    }


    return state;

}
