import React from 'react';
import ReactDOM from 'react-dom';
import {deepPurple900} from 'material-ui/styles/colors';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import store from './redux-config/store';
import { Provider } from 'react-redux'
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import style from './reactstrap.css';
 

injectTapEventPlugin();

import routes from './routes';
import './index.css';



const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#0f3a54',
  },
  appBar: {
    height: 60,
  },
});



ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider muiTheme={muiTheme}>
      {routes}
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
);
