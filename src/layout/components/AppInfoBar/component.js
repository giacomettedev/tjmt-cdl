import React from 'react';

import AppBar from 'material-ui/AppBar';
import SiapLogo from '../SiapLogo';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import Drawer from 'material-ui/Drawer';

import { Card, Button, CardHeader, CardFooter, CardBlock,
    CardTitle, CardText } from 'reactstrap';
 

import {withRouter} from 'react-router';

class AppInfoBar extends React.Component {

    render() {

        const {appInfoOpened} = this.props;

        console.log(appInfoOpened);

        return (
            <Drawer width={350} openSecondary={true} open={!!appInfoOpened} docked={false}  onRequestChange={() => this.props.toggleAppInfo(false) }>

                <AppBar
                    style={{ backgroundColor: '#6ba6bf', borderBottom: '3px solid #ff880e' }}
                    showMenuIconButton={false}
                    titleStyle={{ fontSize: '1.1em' }}
                    title="SOBRE O SISTEMA"
                    iconElementRight={
                        <IconButton style={{ marginTop: '-3px' }} onTouchTap={() => this.props.toggleAppInfo(false) }>
                            <FontIcon className='mdi mdi-close' />
                        </IconButton>
                    }
                    />

                <div className='app-info'>

                    <div className='scroll-container'>
                        <div>
                            <div className='logo-system'>

                                <img src='http://siap2-desenv-a.tjmt.jus.br/assets/logotipos/tjmt.svg' />

                            </div>

                            <Card>
                                <CardHeader>CDL</CardHeader>
                                <CardBlock>
                                    <CardTitle>
                                        Bem vindo ao SIAP – Sistema de Inspeção e Acompanhamento de Produção.
                                    </CardTitle>
                                    <CardText tag="div">

                                        <p> Caso você esteja com duvidas em relação à utilização do sistema e os recursos disponibilizados por este, utilize os tópicos de ajuda relacionados abaixo: </p>

                                        <p> 1) Dúvidas com relação à Administração de Metas, aquelas estabelecidas pelo CNJ ou TJMT, clique aqui! </p>

                                        <p> 2) Dúvidas com relação à Inspeção de Entidades Acolhedoras de Adolescentes Infratores, inspeção esta realizada mensalmente, clique aqui! </p>

                                        <p> 3) Dúvidas relacionadas à Inspeção Virtual, ou seja, relacionadas a como averiguar a situação de Comarcas e Varas ao que tange o “Status” de processos, clique aqui! </p>
                                        <p> Caso você esteja com duvidas em relação à utilização do sistema e os recursos disponibilizados por este, utilize os tópicos de ajuda relacionados abaixo: </p>

                                        <p> 1) Dúvidas com relação à Administração de Metas, aquelas estabelecidas pelo CNJ ou TJMT, clique aqui! </p>

                                        <p> 2) Dúvidas com relação à Inspeção de Entidades Acolhedoras de Adolescentes Infratores, inspeção esta realizada mensalmente, clique aqui! </p>

                                        <p> 3) Dúvidas relacionadas à Inspeção Virtual, ou seja, relacionadas a como averiguar a situação de Comarcas e Varas ao que tange o “Status” de processos, clique aqui! </p>
                                        <p> Caso você esteja com duvidas em relação à utilização do sistema e os recursos disponibilizados por este, utilize os tópicos de ajuda relacionados abaixo: </p>

                                        <p> 1) Dúvidas com relação à Administração de Metas, aquelas estabelecidas pelo CNJ ou TJMT, clique aqui! </p>

                                        <p> 2) Dúvidas com relação à Inspeção de Entidades Acolhedoras de Adolescentes Infratores, inspeção esta realizada mensalmente, clique aqui! </p>

                                        <p> 3) Dúvidas relacionadas à Inspeção Virtual, ou seja, relacionadas a como averiguar a situação de Comarcas e Varas ao que tange o “Status” de processos, clique aqui! </p>
                                        <p> Caso você esteja com duvidas em relação à utilização do sistema e os recursos disponibilizados por este, utilize os tópicos de ajuda relacionados abaixo: </p>

                                        <p> 1) Dúvidas com relação à Administração de Metas, aquelas estabelecidas pelo CNJ ou TJMT, clique aqui! </p>

                                        <p> 2) Dúvidas com relação à Inspeção de Entidades Acolhedoras de Adolescentes Infratores, inspeção esta realizada mensalmente, clique aqui! </p>

                                        <p> 3) Dúvidas relacionadas à Inspeção Virtual, ou seja, relacionadas a como averiguar a situação de Comarcas e Varas ao que tange o “Status” de processos, clique aqui! </p>

                                    </CardText>
                                    <Button>Go somewhere</Button>
                                </CardBlock>
                                <CardFooter>Footer</CardFooter>
                            </Card>
                        </div>
                    </div>

                </div>

            </Drawer> 
        )
    }
}

export default withRouter(AppInfoBar);
