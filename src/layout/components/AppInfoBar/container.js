import {toggleAppInfo} from '../../actions';
import {connect} from 'react-redux';
import component from './component';
import './style.css';

const mapStateToProps = (state) => ({    
    appInfoOpened:  state.layout.appInfoOpened
});

const mapDispatchToProps = (dispatch) => ({
    
    toggleAppInfo(bool) {

        dispatch(toggleAppInfo(bool));
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);