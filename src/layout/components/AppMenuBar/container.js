import {toggleAppMenu} from '../../actions';
import {connect} from 'react-redux';
import component from './component';
import './style.css';

const mapStateToProps = (state) => ({    
    appMenuOpened:  state.layout.appMenuOpened
});

const mapDispatchToProps = (dispatch) => ({
    
    toggleAppMenu(bool) {

        dispatch(toggleAppMenu(bool));
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);