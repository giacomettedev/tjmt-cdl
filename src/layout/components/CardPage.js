import React from 'react';

import Paper from 'material-ui/Paper';

import {Row, Col} from 'reactstrap';



export default (Component) => (props) => {
    
    return (
        <Row>
            <Col >
                <Paper  rounded={false} style={{ paddingTop: "16px", paddingBottom: "16px" }}  ><Component {...props} /></Paper>
            </Col>
        </Row>
    );
};