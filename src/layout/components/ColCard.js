import React from 'react';

import Paper from 'material-ui/Paper';
import {Col} from 'reactstrap';

export default (props) => {
    const {sm, xs, md, lg} = props;
    const colProps = { sm, xs, md, lg };
    const newProps = {...props };

    newProps.style = newProps.style || {};
    newProps.style.padding = newProps.style.padding || "16px";
    newProps.style.margin = newProps.style.margin || "8px";

    return (
        <Col {...colProps} >
            <Paper {...newProps}>
                {props.children}
            </Paper>
        </Col>
    );

};