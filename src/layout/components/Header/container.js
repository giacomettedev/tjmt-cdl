import {toggleAppInfo, toggleAppMenu} from '../../actions';
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({     
});

const mapDispatchToProps = (dispatch) => ({
    
    toggleAppInfo(bool) {

        dispatch(toggleAppInfo(bool)); 
    },
    
    toggleAppMenu(bool) {

        dispatch(toggleAppMenu(bool)); 
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);