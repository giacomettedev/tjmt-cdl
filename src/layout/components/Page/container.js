import {setTitle} from '../../actions';
import {connect} from 'react-redux';


const mapStateToProps = ({ layout: { title, subtitle, icon } }) => ({
    title,
    subtitle,
    icon
});

const mapDispatchToProps = (dispatch) => ({
    onClick() {
        dispatch(setTitle("Teste ASDlkAHSDLKJHALKSJDD"))
    }
});


export default (Page) => connect(
    mapStateToProps,
    mapDispatchToProps
)(Page);