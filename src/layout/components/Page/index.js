import React from 'react';
import withContainer from './container';
import './page.css';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import {Container, Row, Col} from 'reactstrap';
import {withRouter} from 'react-router';


class Page extends React.Component {

    navTo1() {
        this.props.router.push("/details")
    }

    navTo2() {
        this.props.router.push("/")
    }

    render() {

        const {title, subtitle, icon} = this.props;

        return (
            <Container fluid className="page" >
                
                    <Row >
                        <Col>
                            <div className="page-title-container">
                                <div className="page-title-icon-container">
                                    <i className={"mdi mdi-" + (icon || "file") } onClick={this.props.onClick} />
                                </div>
                                <div className="page-title-text-container">
                                    <h1 className="page-title">{title || "Integração CDL"}</h1>
                                    <h2 className="page-sub-title">{subtitle || "Home"}</h2>
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <div className="page-inner">
                        {this.props.children}
                    </div>

                
            </Container>
        );

    }

}

const PageContainer = withContainer(withRouter(Page));

export default ((component) => <PageContainer {...component.props} >{component}</PageContainer>);