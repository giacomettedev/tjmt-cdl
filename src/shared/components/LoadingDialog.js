import React from 'react';
import {connect} from 'react-redux';
import CircularProgress from 'material-ui/CircularProgress';
import Dialog from 'material-ui/Dialog';


class LoadingDialog extends React.Component {

    get style() {
        return {
            container: {
                padding: "32px",
                textAlign: "center"
            },
            indicator: {
                
            },
            title: {
                display: "inline-block",
                fontSize: "32px",
                lineHeight: "32px",
                verticalAlign: "top",
                paddingTop: "32px",
                marginLeft: "16px"
            } 
        };
    }

    render() {
        const {isLoading, loadingText} = this.props;

        return (
            <Dialog open={isLoading} modal>
                <div style={this.style.container} >
                    <CircularProgress size={1.25} style={this.style.indicator}  />
                    <h1 style={this.style.title} >{loadingText || "Carregando..."}</h1>
                </div>
            </Dialog>            
        );

    }

}


const mapStateToProps = (state) => ({
    isLoading: state.layout.loading,
    loadingText: state.layout.loadingText
});

export default connect(mapStateToProps, null)(LoadingDialog); 