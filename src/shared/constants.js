export const BASE_URL = "http://localhost:57077/api";

export const MENU_ITEMS = [
    {
        title: "Integração CDL",
        items: [
            {
                title: 'Consumidor',
                items: [
                    {
                        title: 'Consulta',
                        action: "/consumidor"
                    }
                ]
            },
            {
                title: "Solicitações",
                items: [
                    {
                        title: 'Consulta',
                        action: "/solicitacoes"
                    },
                    {
                        title: 'Nova Solicitação',
                        action: "/solicitacoes/nova"
                    }
                ]
            }
        ]
    },
];



